class Hangman
  attr_reader :guesser, :referee, :board

  def initialize(players)
    @guesser = players[:guesser]
    @referee = players[:referee]
  end

  def setup
    secret_length = @referee.pick_secret_word
    @guesser.register_secret_length(secret_length)
    @board = [nil] * secret_length
  end

  def take_turn
    pick = @guesser.guess(@board)
    indices = @referee.check_guess(pick)
    update_board(pick,indices)
    @guesser.handle_response
  end

  def update_board(guess, array)
    array.each do |indices|
      @board[indices] = guess
    end
  end


end

class HumanPlayer
end

class ComputerPlayer
  attr_accessor :dictionary, :candidate_words

  def initialize(dictionary)
    @dictionary = dictionary
  end

  def pick_secret_word
    @dictionary.join.length
  end

  def check_guess(letter)
    indexes = []
    @dictionary.join.split('').each_with_index do |l,index|
      if l == letter
        indexes.push(index)
      end
    end
    indexes
  end

  # def register_secret_length(length)
  #   puts "Secret is #{length} characters long"
  #   length
  # end
  #
  # def guess(board)
  #   puts "input guess: "
  #   input = gets.chomp
  # end
  #
  # def handle_response(arg1,arg2)
  #   @candidate_words = @dictionary.select {|word| word.length == pick_secret_word}
  # end


  #COME BACK TO BELOW

  def register_secret_length(length)
    # begining to play again; reset candidate_words
    @candidate_words = @dictionary.select { |word| word.length == length }
  end

  def guess(board)


    freq_table = freq_table(board)

    most_frequent_letters = freq_table.sort_by { |letter, count| count }
    letter, _ = most_frequent_letters.last


    letter
  end

  def handle_response(guess, response_indices)
    @candidate_words.reject! do |word|
      should_delete = false

      word.split("").each_with_index do |letter, index|
        if (letter == guess) && (!response_indices.include?(index))
          should_delete = true
          break
        elsif (letter != guess) && (response_indices.include?(index))
          should_delete = true
          break
        end
      end

      should_delete
    end
  end

  def require_secret
    @secret_word
  end

  private
  def freq_table(board)
    # this makes 0 the default value; see the RubyDoc.
    freq_table = Hash.new(0)
    @candidate_words.each do |word|
      board.each_with_index do |letter, index|
        # only count letters at missing positions
        freq_table[word[index]] += 1 if letter.nil?
      end
    end

    freq_table
  end




end
